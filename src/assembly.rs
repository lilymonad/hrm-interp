pub mod parser;

#[derive(Debug)]
pub enum Address {
    Immediate(usize),
    Indirect(usize),
}

impl Address {
    pub fn translate(&self, memory: &mut [Option<i32>]) -> Option<usize> {
        match self {
            Address::Immediate(addr) => Some(*addr),
            Address::Indirect(addr) => memory[*addr].map(|x| x as usize),
        }
    }
}

#[derive(Debug)]
pub enum Instruction {
    In,
    Out,
    Add(Address),
    Sub(Address),
    Jmp(usize),
    JmpIfNeg(usize),
    JmpIfZero(usize),
    Increment(Address),
    Decrement(Address),
    CopyTo(Address),
    CopyFrom(Address),
}
