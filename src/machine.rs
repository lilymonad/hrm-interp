use crate::assembly::Instruction;

#[derive(Debug)]
pub enum Error {
    NoHaltInstruction,
    NothingInMyHands,
    NothingWasSetInTargetMemoryCell,
}

pub struct Machine {
    pub memory: Vec<Option<i32>>,
    pub input: Vec<i32>,
    pub output: Vec<i32>,

    pc: usize,
    acc: Option<i32>,
}

impl Machine {
    pub fn new(memory_size: usize) -> Self {
        Self {
            memory: vec![None; memory_size],
            input: Vec::new(),
            output: Vec::new(),
            pc: 0,
            acc: None,
        }
    }

    fn execute_one(&mut self, instruction: &Instruction) -> Result<(), Option<Error>> {
        let Machine {
            memory,
            input,
            output,
            pc,
            acc,
        } = self;

        match instruction {
            Instruction::In => {
                *acc = Some(input.pop().ok_or(None)?);
            }
            Instruction::Out => {
                output.push(acc.take().ok_or(Error::NothingInMyHands)?);
            }
            Instruction::Add(addr) => {
                let addr = addr
                    .translate(memory)
                    .ok_or(Error::NothingWasSetInTargetMemoryCell)?;
                let x = memory[addr].ok_or(Error::NothingWasSetInTargetMemoryCell)?;
                let y = acc.as_ref().copied().ok_or(Error::NothingInMyHands)?;
                *acc = Some(y + x);
            }
            Instruction::Sub(addr) => {
                let addr = addr
                    .translate(memory)
                    .ok_or(Error::NothingWasSetInTargetMemoryCell)?;
                let x = memory[addr].ok_or(Error::NothingWasSetInTargetMemoryCell)?;
                let y = acc.as_ref().copied().ok_or(Error::NothingInMyHands)?;
                *acc = Some(y - x);
            }
            Instruction::Jmp(addr) => {
                *pc = *addr - 1;
            }
            Instruction::JmpIfNeg(addr) => {
                if acc.as_ref().copied().ok_or(Error::NothingInMyHands)? < 0 {
                    *pc = *addr - 1;
                }
            }
            Instruction::JmpIfZero(addr) => {
                if acc.as_ref().copied().ok_or(Error::NothingInMyHands)? == 0 {
                    *pc = *addr - 1;
                }
            }
            Instruction::Increment(addr) => {
                let addr = addr
                    .translate(memory)
                    .ok_or(Error::NothingWasSetInTargetMemoryCell)?;
                let val = memory[addr].ok_or(Error::NothingWasSetInTargetMemoryCell)? + 1;
                memory[addr] = Some(val);
                *acc = Some(val);
            }
            Instruction::Decrement(addr) => {
                let addr = addr
                    .translate(memory)
                    .ok_or(Error::NothingWasSetInTargetMemoryCell)?;
                let val = memory[addr].ok_or(Error::NothingWasSetInTargetMemoryCell)? - 1;
                memory[addr] = Some(val);
                *acc = Some(val);
            }
            Instruction::CopyTo(addr) => {
                let addr = addr
                    .translate(memory)
                    .ok_or(Error::NothingWasSetInTargetMemoryCell)?;
                let val = acc.as_ref().copied().ok_or(Error::NothingInMyHands)?;
                memory[addr] = Some(val);
            }
            Instruction::CopyFrom(addr) => {
                let addr = addr
                    .translate(memory)
                    .ok_or(Error::NothingWasSetInTargetMemoryCell)?;
                let val = memory[addr].ok_or(Error::NothingWasSetInTargetMemoryCell)?;
                *acc = Some(val);
            }
        }
        *pc += 1;
        Ok(())
    }

    pub fn execute_all(&mut self, instructions: &[Instruction]) -> Result<usize, Error> {
        self.pc = 0;
        self.acc = None;
        let mut n = 0;
        while let Some(instruction) = instructions.get(self.pc) {
            n += 1;
            match self.execute_one(instruction) {
                Err(None) => return Ok(n),
                Err(Some(err)) => return Err(err),
                Ok(()) => {}
            }
        }

        if self.input.is_empty() {
            Ok(n)
        } else {
            Err(Error::NoHaltInstruction)
        }
    }
}
