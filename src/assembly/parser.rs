use std::collections::HashMap;

use nom::{
    character::complete,
    combinator::{complete, recognize},
    error::ParseError,
    multi::fold_many0,
    Err, IResult,
};

use super::{Address, Instruction};
use thiserror;

#[derive(Debug, thiserror::Error)]
pub enum Error<'a> {
    #[error("Parse error: {0}")]
    Nom(nom::Err<nom::error::Error<&'a str>>),
    #[error("Label not found: {0}")]
    LabelNotFound(&'a str),
}

impl<'a> From<nom::Err<nom::error::Error<&'a str>>> for Error<'a> {
    fn from(err: nom::Err<nom::error::Error<&'a str>>) -> Self {
        Error::Nom(err)
    }
}

fn parse_address(input: &str) -> IResult<&str, Address> {
    if let Ok((input, _)) = nom::character::complete::char::<_, nom::error::Error<&str>>('[')(input)
    {
        let (input, address) = complete::u64(input)?;
        let (input, _) = nom::character::complete::char(']')(input)?;
        Ok((input, Address::Indirect(address as usize)))
    } else {
        let (input, address) = complete::u64(input)?;
        Ok((input, Address::Immediate(address as usize)))
    }
}

fn parse_name(input: &str) -> IResult<&str, &str> {
    fn parser(input: &str) -> IResult<&str, ()> {
        let (input, _) = complete::satisfy(|x| x == '_' || x.is_alphabetic())(input)?;
        let (input, _) = fold_many0(
            complete::satisfy(|x| x == '_' || x.is_alphanumeric()),
            || (),
            |_, _| (),
        )(input)?;
        Ok((input, ()))
    }

    recognize(parser)(input)
}

fn parse_label(input: &str) -> IResult<&str, &str> {
    let (input, label) = parse_name(input)?;
    let (input, _) = complete::char(':')(input)?;
    Ok((input, label))
}

fn parse_instruction(input: &str) -> IResult<&str, Intermediate<'_>> {
    let (input, name) = parse_name(input)?;
    match name {
        "outbox" => Ok((input, Intermediate::Instruction(Instruction::Out))),
        "inbox" => Ok((input, Intermediate::Instruction(Instruction::In))),
        "copyfrom" => {
            let (input, _) = complete::multispace0(input)?;
            let (input, address) = parse_address(input)?;
            Ok((
                input,
                Intermediate::Instruction(Instruction::CopyFrom(address)),
            ))
        }
        "copyto" => {
            let (input, _) = complete::multispace0(input)?;
            let (input, address) = parse_address(input)?;
            Ok((
                input,
                Intermediate::Instruction(Instruction::CopyTo(address)),
            ))
        }
        "add" => {
            let (input, _) = complete::multispace0(input)?;
            let (input, address) = parse_address(input)?;
            Ok((input, Intermediate::Instruction(Instruction::Add(address))))
        }
        "sub" => {
            let (input, _) = complete::multispace0(input)?;
            let (input, address) = parse_address(input)?;
            Ok((input, Intermediate::Instruction(Instruction::Sub(address))))
        }
        "bumpup" => {
            let (input, _) = complete::multispace0(input)?;
            let (input, address) = parse_address(input)?;
            Ok((
                input,
                Intermediate::Instruction(Instruction::Increment(address)),
            ))
        }
        "bumpdn" => {
            let (input, _) = complete::multispace0(input)?;
            let (input, address) = parse_address(input)?;
            Ok((
                input,
                Intermediate::Instruction(Instruction::Decrement(address)),
            ))
        }
        "jump" => {
            let (input, _) = complete::multispace0(input)?;
            let (input, label) = parse_name(input)?;
            Ok((input, Intermediate::Jump(Instruction::Jmp, label)))
        }
        "jumpz" => {
            let (input, _) = complete::multispace0(input)?;
            let (input, label) = parse_name(input)?;
            Ok((input, Intermediate::Jump(Instruction::JmpIfZero, label)))
        }
        "jumpn" => {
            let (input, _) = complete::multispace0(input)?;
            let (input, label) = parse_name(input)?;
            Ok((input, Intermediate::Jump(Instruction::JmpIfNeg, label)))
        }
        _ => Err(Err::Error(nom::error::Error::new(
            input,
            nom::error::ErrorKind::Tag,
        ))),
    }
}

enum Intermediate<'a> {
    Jump(fn(usize) -> Instruction, &'a str),
    Instruction(Instruction),
}

pub fn parse_program<'a>(mut input: &'a str) -> Result<Vec<Instruction>, Error<'a>> {
    let mut program = vec![];
    let mut labels = HashMap::new();

    loop {
        let (i, _) = complete::multispace0(input)?;
        if let Ok((i, label)) = parse_label(i) {
            labels.insert(label, program.len());
            input = i;
        } else {
            if let Ok((i, inst)) = parse_instruction(i) {
                program.push(inst);
                input = i;
            } else {
                break;
            }
        }
    }

    program
        .into_iter()
        .map(|inter| {
            Ok(match inter {
                Intermediate::Instruction(inst) => inst,
                Intermediate::Jump(jump, label) => {
                    let x: Result<usize, Error<'a>> = labels
                        .get(&label)
                        .copied()
                        .ok_or(Error::LabelNotFound(label));

                    jump(x?)
                }
            })
        })
        .collect()
}
