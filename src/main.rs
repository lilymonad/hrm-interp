use rand::Rng;

mod assembly;
mod machine;
use assembly::{parser::parse_program, Address, Instruction};

use crate::machine::Machine;

fn main() {
    let mut rng = rand::thread_rng();

    let mut machine = Machine::new(16);
    machine.input = vec![5, 5, 5, 1];

    let zero = 14;
    let diviseur = 15;

    let mut total = 0;

    let program = parse_program(
        r#"
            jump begin

            out:

            add 15
            outbox
            copyfrom 12
            outbox

            begin:

            copyfrom 14
            copyto 12
            inbox

            loop:

            sub 15
            jumpn out
            copyto 13
            bumpup 12
            copyfrom 13
            jump loop
        "#,
    )
    .expect("failed to parse program");

    for _ in 0..1000 {
        machine.memory[diviseur] = Some(4);
        machine.memory[zero] = Some(0);

        machine.input = vec![
            rng.gen_range(0..=15),
            rng.gen_range(0..=15),
            rng.gen_range(0..=15),
            rng.gen_range(0..=15),
        ];

        let result = machine.execute_all(&program);
        match result {
            Ok(n) => {
                total += n;
            }
            Err(err) => {
                println!("error: {:?}", err);
                break;
            }
        }
    }

    println!("average speed: {}", total / 1000);
}
